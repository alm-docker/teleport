FROM multiarch/debian-debootstrap:_DISTCROSS-buster-slim as builder
MAINTAINER Adrien le Maire <adrien@alemaire.be>
RUN apt update && apt search linux-headers | grep -v '^ \|^$\|dev' | sed 's/\/.*//g' | grep -v '\.' | DEBIAN_FRONTEND=noninteractive xargs apt install -y golang git make gcc zip
RUN mkdir -p /root/go/src/github.com/gravitational /var/lib/teleport
ARG GIT_TAG
RUN cd /root/go/src/github.com/gravitational && \
    git clone --single-branch -b $(echo $GIT_TAG | sed 's/v/branch\//' | sed 's/\.[0-9]\+$//') -n https://github.com/gravitational/teleport.git && \
    cd teleport && git checkout $GIT_TAG && \
    env ARCH=_GOCROSS make full


FROM multiarch/debian-debootstrap:_DISTCROSS-buster-slim
MAINTAINER Adrien le Maire <adrien@alemaire.be>
RUN apt-get update && apt-get upgrade -y && \
    apt-get install --no-install-recommends -y \
    dumb-init \
    ca-certificates \
    && update-ca-certificates \
    && rm -rf /var/lib/apt/lists/*
COPY --from=builder /root/go/src/github.com/gravitational/teleport/build/teleport /usr/local/bin/teleport
COPY --from=builder /root/go/src/github.com/gravitational/teleport/build/tctl /usr/local/bin/tctl
COPY --from=builder /root/go/src/github.com/gravitational/teleport/build/tsh /usr/local/bin/tsh

EXPOSE 3022/tcp 3023/tcp 3024/tcp 3025/tcp 3080/tcp
VOLUME ["/var/lib/teleport"]
ENTRYPOINT ["/usr/bin/dumb-init", "teleport", "start", "-c", "/etc/teleport/teleport.yaml"]
